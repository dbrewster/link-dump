/** @jsx React.DOM */

var db = require('script/firebase')
var RandomColorPicker = require('script/random_color_picker')


module.exports = React.createClass({
  mixins: [React.addons.LinkedStateMixin],

  getInitialState: function () {
    return {
      form: "login",
      email: "",
      password: "",
      color: {},
      loading: false,
      signupSucceeded: false
    }
  },

  submitLogin: function (e) {
    e.preventDefault()

    var _this = this

    var authData = {
      email: this.state.email,
      password: this.state.password,
    }
    
    db.login(authData, function (didSucceed) {
      if (didSucceed) _this.props.onAuthenticate()
    })
  },

  submitSignup: function (e) {
    e.preventDefault()

    var _this = this;

    this.setState({loading: true})

    var authData = {
      email: this.state.email,
      password: this.state.password,
      color: this.state.color
    }
    
    db.createUser(authData, function (didSucceed) {
      if (didSucceed) _this.props.onAuthenticate()
    })
  },

  onOverlayClick: function (e) {
    if ($(e.target).closest('.ld-form').length) return
    this.close()
  },

  close: function () {
    this.props.onClose()
  },

  resetState: function () {
    this.setState(this.getInitialState())
  },

  render: function () {

    return (
      <div onClick={this.onOverlayClick} className={React.addons.classSet({
        "overlay": true,
        "is-active": this.props.active,
        "is-loading": this.state.loading
      })}>
        <div className="overlay-inner">
          <div className={React.addons.classSet({
            "auth-form": true,
            "ld-form": true
          })}>
            <div className="ld-form-label">
              <a onClick={this.showLogin} className={this.getLinkClass('login')}>Login</a> / <a onClick={this.showSignup} className={this.getLinkClass('signup')}>Sign Up</a>
            </div>
            {this.renderContent()}
            <div className="ld-form-close" onClick={this.close}>{ '\u00D7' }</div>
          </div>
        </div>
      </div>
    )
  },

  showLogin: function () {
    this.showForm('login')
  },

  showSignup: function () {
    this.showForm('signup')
  },

  showForm: function (formName) {
    this.setState({form: formName})
  },

  getLinkClass: function (linkName) {
    var className = linkName + "-link "
    className += "auth-form-link "
    className += (linkName == this.state.form ? 'is-active' : '')
    return className
  },

  renderContent: function () {
    if (this.state.loginSuccess) { return this.renderSuccess() }
    else return this.renderForm()
  },

  renderForm : function () {
    if (this.state.form == "login") {
      return (
        <div className="login-form">
          <div className="ld-form-controls">
            <div className="ld-form-field-set">
              <input type="text" valueLink={this.linkState('email')} placeholder="Url" />
              <input type="password" valueLink={this.linkState('password')} placeholder="Password" />
            </div>
            <div className="ld-form-submit-container">
              <input type="submit" onClick={this.submitLogin} value={this.state.loading ? 'Loading...' : 'Login'}/>
            </div>
          </div>
        </div>
      )
    } else {
      if (this.state.signupSucceeded) {
        var formContent = (
          <div className="ld-form-success">
            <div className="ld-form-success-text">Account created.</div>
            <a className="ld-form-reset-link" onClick={this.resetState}>Login</a>
          </div>
        )
      } else {
        var formContent = (
          <div className="ld-form-controls">
            <div className="ld-form-field-set">
              <RandomColorPicker valueLink={this.linkState('color')} />
              <input type="text" valueLink={this.linkState('email')} placeholder="Email" />
              <input type="password" valueLink={this.linkState('password')} placeholder="Password" />
            </div>
            <div className="ld-form-submit-container">
              <input type="submit" onClick={this.submitSignup} value={this.state.loading ? 'Loading...' : 'Sign Up'}/>
            </div>
          </div>
        )
      }

      return (
        <div className="signup-form">
          {formContent}
        </div>
      )
    }
  }
})