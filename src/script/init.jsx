/** @jsx React.DOM */

db = require('script/firebase')
AppView = require('script/app_view')

$(document).ready(function () {
  db.checkSession(function (isAuthorized) {
    React.renderComponent(<AppView authorized={isAuthorized} />, $('#content')[0])
  })
})