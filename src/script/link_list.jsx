/** @jsx React.DOM */

var db = require('script/firebase')
var Link = require('script/link')

module.exports = LinkList = React.createClass({
  getInitialState: function () {
    return {links: []}
  },

  componentDidMount: function () {
    var _this = this;

    db.root.child('links').on('value', function (snapshot) {
      var data = snapshot.val()
      var links = []

      for (key in data) {
        if (data.hasOwnProperty(key)) {
          links.push(data[key])
        }
      }

      links.reverse()

      _this.setState({links: links})
    })
  },

  render: function () {
    var linkNodes = this.state.links.map(this.renderLink)

    return (
      <div className="link-list">
        {linkNodes}
      </div>
    )
  },

  renderLink: function (link) {
    return (<Link model={link}/>)
  }

})