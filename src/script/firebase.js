var db = new Firebase("https://cilinks.firebaseio.com/");

module.exports = {
  root: db,

  checkSession: function (cb) {
    var _this = this

    this.execAuthFunction('checkSession', null, function (error, user) {
      if (user) {
        db.child('users').child(user.id).once('value', function (user) {
          _this.user = user.val()
          cb(true)
        })
      } else {
        cb(false)
      }
    })
  },

  login: function (options, cb) {
    var _this = this

    this.execAuthFunction('login', options, function (error, user) {
      if (user) {
        _this.user = user
        cb(true)
      } else {
        cb(false)
      }
    })

  },

  logout: function (cb) {
    this.execAuthFunction('logout')
    cb(true)
  },

  createUser: function (options, cb) {
    var _this = this

    this.execAuthFunction('createUser', options, function (error, user) {
      _this.login(options, function (didSucceed) {
        if (didSucceed){
          db.child('users').child(_this.user.id).set({email:options.email, color:options.color}, function () {
            db.child('users').child(_this.user.id).once('value', function (user) {
              console.log('made')
              _this.user = user.val()
              cb(true)
            })
          })
        } else {
          cb(false)
        }
      })
    })
  },

  execAuthFunction: function (fnString, options, cb) {
    var _this = this

    this.authHandlers = this.authHandlers || []

    if (fnString != "createUser") {
      this.authHandlers.push(cb)
    }

    if (!this.authRef && fnString != "checkSession") {
      var ignoreFirst = true
    }

    if (!this.authRef) {
      this.authRef = new FirebaseSimpleLogin(db, function (error, user) {
        if (ignoreFirst) {
          ignoreFirst = false
          return
        }

        while (_this.authHandlers.length) {
          _this.authHandlers.pop()(error, user)
        }
      })
    }

    if (fnString == "login") {
      this.authRef.login('password', options)
    } else if (fnString == "createUser") {
      this.authRef.createUser(options.email, options.password, cb)
    } else if (fnString == "logout") {
      this.authRef.logout()
    }
  }
}