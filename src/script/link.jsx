/** @jsx React.DOM */


module.exports = Link = React.createClass({
  
  getInitialState: function () {
    return { isMounted: false }
  },

  componentDidMount: function () {
    setTimeout(function () {
      this.setState({isMounted: true})
    }.bind(this))
  },  

  render: function () {
    var link = this.props.model
    var c = link.color || {r: 0, g: 0, b: 0}

    var linkStyle = {
      backgroundColor: 'rgb(' + [c.r, c.g, c.b].join(',') + ')',
      opacity: (this.state.isMounted ? 1 : 0)
    }

    console.log(linkStyle)

    return (
      <div className="link" style={linkStyle}>
        <a href={link.text}>{link.text}</a>
        <div className="link-username">{link.username}</div>
      </div>
    )
  }
})