/** @jsx React.DOM */

var AddLinkForm = require('script/add_link_form')
var AuthForm = require('script/auth_form')
var LinkList = require('script/link_list')
var db = require('script/firebase')

module.exports = AppView = React.createClass({
  getInitialState: function () {
    return {
      user: null,
      adding: false,
      authorizing: this.props.authorizing
    }
  },

  toggleAuthForm: function () {
    this.setState({authorizing: !this.state.authorizing})
  },

  toggleAddForm: function () {
    this.setState({adding: !this.state.adding})
  },

  setUser: function (user) {
    this.setState({
      authorizing: false,
      user: user
    })

    this.setProps({authorized: true})
  },

  logout: function () {
    var _this = this

    db.logout(function () {
      _this.setProps({authorized: false})
    })
  },

  render: function () {
    if (!this.props.authorized) {
      var loginLink = <a className="login-link" onClick={this.toggleAuthForm}>Login / Register</a>
    } else {
      var loginLink = <a className="logout-link" onClick={this.logout}>Logout</a>
    }

    return (
      <div className="application">
        <div className="auth-links">
          {loginLink}
        </div>
        <AddLinkForm active={this.state.adding} onAdd/>
        <AuthForm active={this.state.authorizing} onClose={this.toggleAuthForm} onAuthenticate={this.setUser}/>
        <LinkList />
      </div>
    )
  }
})

