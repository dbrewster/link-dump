/** @jsx React.DOM */

var db = require('script/firebase')

module.exports = AddLinkForm = React.createClass({

  getInitialState: function () {
    return {value: "", valid: true}
  },

  onKeyUp: function (e) {
    if (e.keyCode==13) this.submit()
  },

  onChange: function (e) {
    var isValid = !!this.getFormattedLink() || this.state.value.length < 3

    this.setState({
      value: e.target.value,
      valid: isValid
    })
  },

  submit: function () {
    var link = this.getFormattedLink()
    if (!link) return this.setState({valid: false})

    linkRef = db.root.child('links').push()
    linkRef.set({
      text: link,
      username: db.user.email.split('@').shift(),
      color: db.user.color
    })
    this.setState({value: ""})
  },

  getFormattedLink: function () {
    var val = this.state.value

    if ($.trim(val).match(/\s/g)) return false

    var suffix = val.split('.').pop()
    if (suffix == val || suffix.length > 3 || suffix.length < 2) return false

    var protocol = val.split('://').shift()
    if (protocol != val && protocol != 'http' && protocol != 'https') return false

    if (protocol==val) val = 'http://' + val

    return val;
  },
 
  render: function () {
    return (
      <div className={React.addons.classSet({
        "add-link-form": true,
        "is-active": this.props.active,
        "is-invalid": !this.state.valid
      })}>
        <input onChange={this.onChange} onKeyUp={this.onKeyUp} type="text" placeholder="url" value={this.state.value} />
      </div>
    )
  }
})