require('coffee-script')

var gulp = require('gulp')
var sass = require('gulp-sass')
var concat = require('gulp-concat')
var react = require('gulp-react')
var browserify = require('gulp-browserify')
var ee = require('streamee')
var define = require('gulp-wrap-define')
var nodemon = require('gulp-nodemon')
var express =require('express')


gulp.task('default', function () {
  gulp.run('script', 'style', 'assets', 'server')

  // Watch files and run tasks if they change
  gulp.watch(['./src/**/*.jsx', './src/**/*.js'], function() {
    gulp.run('script');
  });

  gulp.watch('./src/**/*.scss', function() {
    gulp.run('style');
  });

  gulp.watch('./src/assets/**', function() {
    gulp.run('assets');
  });

  
})

gulp.task('server', function () {
  express()
    .use(express.static('./public'))
    .listen(3333);
})

gulp.task('style', function () {
    gulp.src('./src/**/*.scss')
        .pipe(sass())
        .pipe(concat("app.css"))
        .pipe(gulp.dest('./public'))
        .on('error', function (e) {console.log(e.message)});
});

gulp.task('script', function () {
  ee.interleave([
    gulp.src('./src/**/*.jsx').pipe(react()).on('error', function (e) {console.log(e.message)}),
    gulp.src('./src/**/*.js')
  ])
    .pipe(define({root: "./src", define:"require.register"}))
    .pipe(concat('app.js'))
    .pipe(gulp.dest('./public'))
})

gulp.task('assets', function () {
  gulp.src('./src/assets/**')
    .pipe(gulp.dest('./public'))
})