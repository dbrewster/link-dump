
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});


Parse.Cloud.beforeSave("Artifact", function (request, response) {
  var text = request.object.get('description')
  var tagNames = text.match(/#(.+?)(?: |$)/g).map(function (tag) {
    tag = tag.replace(' ', '')
  });

  var Tag = Parse.Object.extend('Tag')
  relation = request.object.relation('tags')

  var tagQuery = new Parse.Query(Tag)
  query.equalTo('slug', request.object.get('slug'))
  query.first()
    .done(function (item) {
      
    })
    .error(function () {
      
    })

  response.success()
})

function tagQuery () {

}