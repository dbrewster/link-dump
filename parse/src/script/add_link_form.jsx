/** @jsx React.DOM */

var artifactService = require('script/artifact_service')
var userService = require('script/user_service')


module.exports = AddLinkForm = React.createClass({

  getInitialState: function () {
    return {
      valid: true,
      description: "",
      url: ""
    }
  },

  onKeyUp: function (e) {
    if (e.keyCode==13) this.submit()
  },

  onChange: function (e) {
    this.setState({
      url: $(this.getDOMNode()).find('[name=url]').val(),
      description: $(this.getDOMNode()).find('[name=description]').val()
    })
  },

  submit: function () {
    var link = this.getFormattedLink()
    if (!link) return this.setState({valid: false})

    artifactService.create({
      url: this.state.url,
      description: this.state.description
      tags: this.extractTags(this.state.desc)
    })

    this.setState({value: ""})
  },

  getFormattedLink: function () {
    var val = this.state.url
    // if ($.trim(val).match(/\s/g)) return false
    var suffix = val.split('.').pop()
    // if (suffix == val || suffix.length > 3 || suffix.length < 2) return false
    var protocol = val.split('://').shift()
    // if (protocol != val && protocol != 'http' && protocol != 'https') return false
    if (protocol==val) val = 'http://' + val

    return val;
  },
 
  render: function () {

    return (
      <div 
        className={React.addons.classSet({
          "add-link-form": true,
          "is-active": this.props.active,
          "is-invalid": !this.state.valid
        })}
        onChange={this.onChange} 
        onKeyUp={this.onKeyUp} 
      >
        <input type="text" name="url" placeholder="url" value={this.state.url} />
        <textarea name="description" placeholder="description and #tags">
          {this.state.description}
        </textarea>
        <input type="submit" onClick={this.submit} />
      </div>
    )
  }
})