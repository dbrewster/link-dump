/** @jsx React.DOM */

var RandomColorPicker = require('script/random_color_picker')
var userService = require('script/user_service')


module.exports = React.createClass({
  mixins: [React.addons.LinkedStateMixin],

  getInitialState: function () {
    return {
      form: "login",
      email: "",
      password: "",
      color: {},
      loading: false,
      signupSucceeded: false
    }
  },

  submitLogin: function (e) {
    e.preventDefault()

    var _this = this

    var username = this.state.email
    var password = this.state.password
    
    userService.login(username, password)
      .done(function () {
        _this.props.onAuthenticate()
      })
  },

  submitSignup: function (e) {
    e.preventDefault()

    var _this = this;

    this.setState({loading: true})

    var email = this.state.email;
    var password = this.state.password;
    var color = this.state.color;
    
    userService.createAccount(email, password, color)
      .always(function () {
        _this.setState({loading: false})
      })
  },

  close: function () {
    this.props.onClose()
  },

  resetState: function () {
    this.setState(this.getInitialState())
  },

  render: function () {
    return (
      <div className="overlay">
        <div className="overlay-inner">
          <div className={React.addons.classSet({
            "auth-form": true,
            "ld-form": true
          })} onKeyPress={this.maybeSubmitLogin}>
            <div className="ld-form-label">
              <a onClick={this.showLogin} className={this.getLinkClass('login')}>Login</a> / <a onClick={this.showSignup} className={this.getLinkClass('signup')}>Sign Up</a>
            </div>
            {this.renderContent()}
          </div>
        </div>
      </div>
    )
  },

  maybeSubmitLogin: function (e) {
    if (e.charCode == 13) {
      this.submitLogin(e)
    }
  },

  showLogin: function () {
    this.showForm('login')
  },

  showSignup: function () {
    this.showForm('signup')
  },

  showForm: function (formName) {
    this.setState({form: formName})
  },

  getLinkClass: function (linkName) {
    var className = linkName + "-link "
    className += "auth-form-link "
    className += (linkName == this.state.form ? 'is-active' : '')
    return className
  },

  renderContent: function () {
    if (this.state.loginSuccess) { return this.renderSuccess() }
    else return this.renderForm()
  },

  renderForm : function () {
    if (this.state.form == "login") {
      return (
        <div className="login-form">
          <div className="ld-form-controls">
            <div className="ld-form-field-set">
              <input type="text" valueLink={this.linkState('email')} placeholder="Email" />
              <input type="password" valueLink={this.linkState('password')} placeholder="Password" />
            </div>
            <div className="ld-form-submit-container">
              <input type="submit" onClick={this.submitLogin} value={this.state.loading ? 'Loading...' : 'Login'}/>
            </div>
          </div>
        </div>
      )
    } else {
      if (this.state.signupSucceeded) {
        var formContent = (
          <div className="ld-form-success">
            <div className="ld-form-success-text">Account created.</div>
            <a className="ld-form-reset-link" onClick={this.resetState}>Login</a>
          </div>
        )
      } else {
        var formContent = (
          <div className="ld-form-controls">
            <div className="ld-form-field-set">
              <RandomColorPicker valueLink={this.linkState('color')} />
              <input type="text" valueLink={this.linkState('email')} placeholder="Email" />
              <input type="password" valueLink={this.linkState('password')} placeholder="Password" />
            </div>
            <div className="ld-form-submit-container">
              <input type="submit" onClick={this.submitSignup} value={this.state.loading ? 'Loading...' : 'Sign Up'}/>
            </div>
          </div>
        )
      }

      return (
        <div className="signup-form">
          {formContent}
        </div>
      )
    }
  }
})