var utils = require('script/utils')


module.exports = {

  createAccount: function (username, password, color) {
    var user = new Parse.User();

    user.set({
      username: username,
      password: password,
      color: color
    });

    return user.signUp();
  },

  login: function (username, password) {
    return Parse.User.logIn(username, password);
  },

  logout: function () {
    Parse.User.logOut();

    // Blech for API consistency
    var d = utils.deferred();

    setTimeout(function () {
      d.resolve();
    });

    return d;
  },

  current: function () {
    return Parse.User.current();
  }

}