/** @jsx React.DOM */

module.exports = RandomColorPicker = React.createClass({
  transitionSteps: 30,
  stepDuration: 15,

  getInitialState: function () {
    return {
      currentColor: this.randColor(),
      nextColor: this.randColor(),
      step: 0,
      dirty: false
    }
  },

  start: function () {
    var _this = this

    this.interval = setInterval(function () {
      _this.step()
    }, this.stepDuration)
  },

  stop: function () {
    clearInterval(this.interval)
  },

  step: function () {
    var opts = {dirty: true}

    var stepsRemaining = this.transitionSteps - this.state.step
    var col = this.state.currentColor
    var nextCol = this.state.nextColor

    opts.currentColor = {
      r: Math.floor(col.r + (nextCol.r - col.r) * (1 / stepsRemaining)),
      g: Math.floor(col.g + (nextCol.g - col.g) * (1 / stepsRemaining)),
      b: Math.floor(col.b + (nextCol.b - col.b) * (1 / stepsRemaining))
    }

    if (this.state.step == this.transitionSteps - 1) {
      opts.nextColor = this.randColor()
      opts.step = 0
    } else {
      opts.step = ++this.state.step
    }

    this.setState(opts)
    this.props.valueLink.requestChange(this.state.currentColor)
  },

  randColor: function () {
    return {
      r: this.rand(0, 255),
      g: this.rand(0, 255),
      b: this.rand(0, 255),
    }
  },

  rand: function (min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  },

  currentColorVal: function () {
    var col = this.state.currentColor
    return 'rgb(' + [col.r, col.g, col.b].join(',') + ')'
  },

  render: function () {
    var style = { 
      backgroundColor: this.currentColorVal(),
      display: this.state.dirty ? 'block' : 'none'
    }
    return (
      <div className="random-color-picker" onMouseDown={this.start} onMouseUp={this.stop}>
        <div className="rcp-swatch" style={style}></div>
        <div className="rcp-label">
          <div className="rcp-label-inner">
            Click and hold to select a color
          </div>
        </div>
      </div>
    )
  }
})