/** @jsx React.DOM */

var artifactService = require('script/artifact_service')
var Link = require('script/link')
var AddLink = require('script/add_link')


module.exports = LinkList = React.createClass({
  
  getInitialState: function () {
    return {links: []}
  },

  componentDidMount: function () {
    var _this = this;

    artifactService.all()
      .done(function (artifacts) {
        _this.setState({links: artifacts})
      })
  },

  render: function () {
    var linkNodes = this.state.links.map(this.renderLink)

    return (
      <div className="link-list">
        <AddLink />
        {linkNodes}
      </div>
    )
  },

  renderLink: function (link) {
    return (<Link model={link}/>)
  }
  
})