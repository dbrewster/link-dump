var Artifact = require('script/artifact')
var userService = require('script/user_service')


module.exports = {

  create: function (opts) {
    artifact = new Artifact(opts);
    artifact.set('parent', userService.current());
    return artifact.save();
  },

  all: function () {
    var query = new Parse.Query(Artifact)
    query.include('parent');
    query.descending("createdAt");
    return query.find();
  }
  
}