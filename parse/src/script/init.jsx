/** @jsx React.DOM */

// db = require('script/firebase')
require('script/parse')
AppView = require('script/app_view')

$(document).ready(function () {
  React.renderComponent(<AppView />, $('#content')[0])
})