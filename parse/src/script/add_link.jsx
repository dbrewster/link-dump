/** @jsx React.DOM */

var AddLinkForm = require('script/add_link_form')


module.exports = AddLink = React.createClass({
    
    getInitialState: function () {
      return {
        activeForm: false
      }
    },

    toggleForm: function () {
      this.setState({
        activeForm: !this.state.activeForm
      })
    },

    hideForm: function () {
      this.setState({
        activeForm: true
      })
    },

    render: function () {
      return (
        <div className={React.addons.classSet({
          "add-link": true,
          "is-active": this.state.activeForm
        })}>
          <AddLinkForm active={true} onClose={this.hideForm} />
          <a className="add-link-button" onClick={this.toggleForm}>+</a>
        </div>
      )
    }

})