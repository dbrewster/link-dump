/** @jsx React.DOM */

var Artifact = require('script/artifact')
var AuthForm = require('script/auth_form')
var userService = require('script/user_service')
var AddLinkForm = require('script/add_link_form')
var LinkList = require('script/link_list')


module.exports = AppView = React.createClass({

  getInitialState: function () {
    return {
      authorized: userService.current()
    }
  },

  logout: function () {
    var _this = this;

    userService.logout().done(function () {
      _this.setState({
        authorized: false
      })
    })
  },

  onLogin: function () {
    this.setState(this.getInitialState())
    this.render()
  },

  render: function () {
    return (
      <div className="application">
        <div className="nav-bar">
            <div className="page-title">WO / Links</div>

            { this.state.authorized ?
              <div className="auth-links">
                <a className="logout-link" onClick={this.logout}>Logout</a>
              </div>
            : null }
        </div>

        { this.state.authorized ? 
          <LinkList />
        : <AuthForm active={true} onAuthenticate={this.onLogin}/> }
      </div>
    )
  }

})
